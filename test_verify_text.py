from urllib.request import urlopen
from bs4 import BeautifulSoup

import ssl

# Fixes the ssl certificate problem that the url gives
ssl._create_default_https_context = ssl._create_unverified_context

url = "https://the-internet.herokuapp.com/context_menu"
html = urlopen(url).read()
soup = BeautifulSoup(html, features="html.parser")

# Rermove all script and style elements
for script in soup(["script", "style"]):
    script.extract()   

# Now get the text
text = soup.get_text()

desired_text = "Right-click in the box below to see one called 'the-internet'"
desired_text2 = "Alibaba"

# Tests
def test_desired_text_exists():
    assert (desired_text in text)

def test_desired_text2_exists():
    assert (desired_text2 in text)