# f5-project

This project uses the Beautiful Soup and urllib3 packages to search a web page for specific text.

The desired text is:

1."Right-click in the box below to see one called 'the-internet'", and 2. "Alibaba"

There are two tests written with Pytest, one for each of the desired texts. For each test, if the text is found on the webpage, the test will pass. Otherwise, it will fail.

The environemnt in which we run the tests is defined in the .gitlab-ci.yml file. We create a Python Docker image, install all the necessary dependencies and then run the tests.

To run the project, run the `pytest` command from the root directory of the project.
